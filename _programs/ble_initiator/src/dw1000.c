/*=====dw1000===========================================================*/

/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/06/15
 *===========================================================================*/

/*=====[Inclusion of own header]=============================================*/

#include "dw1000.h"

/*=====[Inclusions of private function dependencies]=========================*/

#include "nrf_gpio.h"
#include "dw1001_dev.h"
#include "uart.h"
#include "port_platform.h"

/*=====[Definition macros of private constants]==============================*/

/* Delay between frames, in UWB microseconds. See NOTE 1 below. */
#define POLL_TX_TO_RESP_RX_DLY_UUS 100

/*=====[Private function-like macros]========================================*/

/*=====[Definitions of private data types]===================================*/

/*=====[Definitions of external public global variables]=====================*/

/*=====[Definitions of public global variables]==============================*/

/*=====[Definitions of private global variables]=============================*/

/*=====[Prototypes (declarations) of external public functions]==============*/

/*=====[Prototypes (declarations) of private functions]======================*/

/*=====[Implementations of public functions]=================================*/

void dw1000_init(dwt_config_t config) {
//-------------dw1000  ini------------------------------------

	/* Setup DW1000 IRQ pin */
	nrf_gpio_cfg_input(DW1000_IRQ, NRF_GPIO_PIN_NOPULL); 		//irq

	/*Initialization UART*/
	boUART_Init();
	printf("Singled Sided Two Way Ranging Initiator Example \r\n");

	/* Reset DW1000 */
	reset_DW1000();

	/* Set SPI clock to 2MHz */
	port_set_dw1000_slowrate();

	/* Init the DW1000 */
	if (dwt_initialise(DWT_LOADUCODE) == DWT_ERROR) {
		//Init of DW1000 Failed
		while (1) {
		};
	}

	// Set SPI to 8MHz clock
	port_set_dw1000_fastrate();

	/* Configure DW1000. */
	dwt_configure(&config);

	/* Apply default antenna delay value. See NOTE 2 below. */
	dwt_setrxantennadelay(RX_ANT_DLY);
	dwt_settxantennadelay(TX_ANT_DLY);

	/* Set preamble timeout for expected frames. See NOTE 3 below. */
	//dwt_setpreambledetecttimeout(0); // PRE_TIMEOUT
	/* Set expected response's delay and timeout.
	 * As this example only handles one incoming frame with always the same delay and timeout, those values can be set here once for all. */
	dwt_setrxaftertxdelay(POLL_TX_TO_RESP_RX_DLY_UUS);
	dwt_setrxtimeout(65000); // Maximum value timeout with DW1000 is 65ms

	//-------------dw1000  ini------end---------------------------
}
/*=====[Implementations of interrupt functions]==============================*/

/*=====[Implementations of private functions]================================*/

